""" Overwrites the logging handler in the 'logging' module. """
from logging.handlers import RotatingFileHandler


class MyRotatingFileHandler(RotatingFileHandler):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        if self.backupCount > 0:
            raise ValueError('Cannot set backupCount, as it is determined automagically')

        self._file_extensions = ['.old', '.older', '.older2', '.oldest']
        self.backupCount = len(self._file_extensions)


    def rotation_filename(self, default_name: str):

        index = int(default_name.split('.')[-1])
        base_filename = ''.join(default_name.split('.')[0:-1])
        try:
            index = self._file_extensions[index]
        except IndexError:
            return super().rotation_filename(default_name)

        filename = base_filename + index
        return filename