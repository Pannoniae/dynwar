"""
Distance/hex conversion stuff.

"""

from math import pi, cos, sin


def hex_corner(center, size, i):
    angle_deg = 60 * i
    angle_rad = pi / 180 * angle_deg
    return (round(center[0] + size * cos(angle_rad), 2),
            round(center[1] + size * sin(angle_rad), 2))


def all_hex_corners(center, size):
    result = []
    for i in range(0, 6):
        result.append(hex_corner(center, size, i))
    return result


"""

Preserved for historical reasons.

def cube_to_offset(cube):
    x, y, z = cube
    row = z + (x + (x&1)) / 2
    col = x
    return (row, col)

def offset_to_cube(hex):
    x = hex[1]
    z = hex[0] - (hex[1] + (hex[1]&1)) / 2
    y = -x - z
    return (x, y, z)
    
def offset_to_pixel(hex, size):
    x = size * 3/2 * hex[1]
    y = size * sqrt(3) * (hex[0] - 0.5 * (hex[1]&1))
    return (x, y)

def pixel_to_offset(x, y, size):
    q = x * 2/3 / size
    r = (-x / 3 + sqrt(3)/3 * y) / size
    return cube_to_hex(cube_round((q, -q - r, r)))
    
def offset_round(hex):
    return cube_to_offset(cube_round(offset_to_cube(hex)))

def offset_distance(hex, other):
    a_cube = offset_to_cube(hex.pos)
    b_cube = offset_to_cube(other.pos)
    return cube_distance(a_cube, b_cube)

distance = offset_distance
    
    """


def cube_to_hex(hex):  # axial
    q = hex[0]
    r = hex[2]
    return q, r


def hex_to_cube(hex):  # axial
    x = hex[0]
    z = hex[1]
    y = -x - z
    return x, y, z


def cube_round(hex):
    rx = round(hex[0])
    ry = round(hex[1])
    rz = round(hex[2])

    x_diff = abs(rx - hex[0])
    y_diff = abs(ry - hex[1])
    z_diff = abs(rz - hex[2])

    if x_diff > y_diff and x_diff > z_diff:
        rx = -ry - rz
    elif y_diff > z_diff:
        ry = -rx - rz
    else:
        rz = -rx - ry

    return rx, ry, rz


def cube_distance(hex, other):
    return (abs(hex[0] - other[0]) + abs(hex[1] - other[1]) + abs(hex[2] - other[2])) / 2
