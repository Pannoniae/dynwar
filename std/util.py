#  various utility functions
import logging
from random import randint, normalvariate, random
from typing import Tuple

import sys


def norandom() -> int:
    """ Disable random numbers, just return 0. """

    return 0

def norandom_multiplier() -> int:

    return 1  # maybe used for multipliers


def uniform(range = 2) -> int:
    """ Uniform distribution of random numbers. """

    return randint(-range, range)


def normvariate(multiplier = 1) -> float:
    """ A random function used to retrieve random numbers.

        Returns a float, smaller or larger than 0. """

    return normalvariate(0, 1) * multiplier

def rand() -> float:
    """ Random value from 0 to 1."""

    return random()


# Convert functions.

def parse_coords(position: str) -> Tuple[int, int]:
    from dn.io.loader import DataSyntaxError

    position = position.strip()
    try:
        x, y = map(int, position.split(','))
    except ValueError:
        # this is not a entry for a hexagon
        raise DataSyntaxError('{} is not a position'.format(position))
    return x, y

def parse_population(population: str):
    from dn.io.loader import DataError

    """ Parse a string, formatted like '2M', '300K' """

    population = population.strip()
    suffix = population[-1:]
    prefix = population[:-1]
    if suffix.isdigit():  # so the population was less than 1000. Those poor bastards...
        suffix = ''
        prefix = population

    lookup = {
        '': 1,
        'K': 10 ** 3,
        'M': 10 ** 6,
        'B': 10 ** 9,
        'T': 10 ** 12,
    }

    significant = int(prefix)
    if not (1 <= significant <= 1000):
        raise DataError("Invalid range for population multiplier (%s). It should be between 1~999." % prefix)

    try:
        exponent = lookup[suffix]
    except KeyError:  # so we were idiots
        raise DataError("Invalid suffix for number %s" % population)

    return significant * exponent

def exc_info(e: Exception):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    return '<exception type {exc}, args {args}, with cause {cuz}, ctx {ctx}, lineno {ln}>'\
           ''.format(exc = exc_type, args = e.args, cuz = e.__cause__, ctx = e.__context__, ln = exc_tb.tb_lineno)


def report(inst):

    """ Prints useful and less useful info about the program. """

    for attribute in vars(inst):
        logging.info(f'=== {attribute} ===')

        try:
            for member in eval('inst.' + attribute):
                logging.info(f'{member}')
        except TypeError:  # so this is not iterable it seems
            logging.debug(f'{attribute} is not iterable, sorry.')


def heuristic(a, b):
    # Manhattan distance on a square grid
    return abs(a.x - b.x) + abs(a.y - b.y)