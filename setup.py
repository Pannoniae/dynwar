# you have no rights to the code til we decide on a licence

# be patient :)

from setuptools import setup

with open('README.md') as f:
    long_description = f.read()

setup(
    name='dynwar',
    version='0.1',
    description='A hex-based wargame',
    license="You have no rights",
    long_description=long_description,
    author='Zsombor and Miklós Prisznyák',
    author_email='zsombor.prisznyak@gmail.com',
    url="https://bitbucket.org/Pannoniae/dynwar/",
    packages=['dn'],
    install_requires=['ruamel.yaml'],  # external packages as dependencies
    scripts=[
        'main.py',
    ],
    # https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console', # yet
        'Intended Audience :: End Users/Desktop',
        'License :: You have no damn rights here',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Games/Entertainment :: Turn Based Strategy'
    ],
)
