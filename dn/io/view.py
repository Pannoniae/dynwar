"""
  Game specific view class
"""
import pygame
from .eventhandler import EventHandler


class View(EventHandler):
    """
      user interface

      it handles a resizable PyGame window, the keyboard and the mouse
    """

    def __init__(self, width, height, caption):
        pygame.init()
        super().__init__()
        self.width = width
        self.height = height
        self.caption = caption
        self.clock = pygame.time.Clock()
        self.fps = 30
        # the data model must be connected by the controller
        self.model = None
        self.canvas = None
        self.canvas = pygame.display.set_mode((self.width, self.height), pygame.RESIZABLE)
        self.canvas.fill((255, 255, 255))  # white
        # self.shrink_x = float(self.width) / self.model.SIZE_X
        # self.shrink_y = float(self.height) / self.model.SIZE_Y

    def run(self):
        self.watch_events()

    def draw(self):
        pass

    def display_fps(self):
        self.clock.tick(self.fps)
        fps = " fps: %.2f " % self.clock.get_fps()
        pygame.display.set_caption(self.caption + fps)

    def tick(self):
        self.model.update()
        self.draw()
        self.display_fps()

    def on_exit(self):
        print("Shutting down...")
        # now call the implementation in EventHandler
        super(View, self).on_exit()

    def on_resize(self, event):
        self.width = event.w
        self.height = event.h

    def on_key_down(self, event):
        #if event.key == pygame.key.K_ESCAPE:
            #self.on_exit()
        pass
