import logging
from collections import defaultdict
from os.path import join, isdir, commonprefix
from ruamel.yaml import YAML, YAMLError, MarkedYAMLError

from dn.game.map.area import Area
from dn.game.config import DATA_PATH
from dn.game.map.country import Country
from dn.game.map.hexmap import Position, Hex, PositionError, Hexmap
from dn.game.map.terrain import TerrainType


class BaseLoader(object):

    # Data sets
    map = ['countries', 'areas', 'map', 'units']


    def __init__(self, directory=DATA_PATH):
        self.directory = directory
        self.yaml = YAML()
        self.data = dict()

    def read(self, filename: str):
        fn = join(self.directory, filename + '.yml')
        try:
            with open(fn) as f:
                logging.info('reading file {}...'.format(filename))
                loaded_data = self.yaml.load(f)
                if loaded_data is None:
                    logging.warning('Empty data in {}, please check file!'.format(filename))
                return loaded_data
        except (YAMLError, MarkedYAMLError):
            logging.error(f'Invalid yaml map in {fn}')
        except FileNotFoundError:
            logging.error(f"Couldn't open file {fn}. Please check file existence!")
        except Exception as e:
            from std.util import exc_info
            logging.error(f'''An unknown error occurred in {fn}. Check that more carefully, bro!\n'
                          'Some more information about this error:\n'
                          {exc_info(e)}''')

    def load(self, data):
        for item in data:
            self.data[item] = self.read(item)

    def set_directory(self, raw_directory: str, relative = True):


        if relative:
            if not isdir(join(DATA_PATH, raw_directory)):
                logging.warning(f'{raw_directory} is not a valid directory, setting to default...')
                self.directory = DATA_PATH
                return
            directory = join(DATA_PATH, raw_directory)
            if commonprefix((directory, DATA_PATH)) != DATA_PATH:
                logging.critical("You are a malicious bastard. 'nuff said.")
        else:
            if not isdir(raw_directory):
                logging.warning(f'{raw_directory} is not a valid directory, setting to default...')
                self.directory = DATA_PATH
                return
            logging.warning(
                "Loader directory is set to a location outside the game's folders. This may cause security problems.")
            directory = raw_directory
            self.directory = DATA_PATH
        self.directory = directory


class WorldLoader(BaseLoader):

    def __init__(self, world, scenario: str, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.world = world

        # input declarations
        self.set_directory(scenario)
        self.load(self.map)
        self._hexmap = self.data['map']
        self._units = self.data['units']
        self._areas = self.data['areas']
        self._countries = self.data['countries']

        # output declarations
        self.hexmap = Hexmap(self._hexmap['size_x'], self._hexmap['size_y'])
        self.units = defaultdict(set)
        self.areas = dict()
        self.countries = dict()
        self.init_data()

    def init_data(self):

        """ Init data needed by World. """
        self._init_countries()
        self._init_areas()
        self._init_hexmap()
        self._init_units()

    def get_values_huac(self):
        """ Returns loaded data in HUAC order. """
        return self.hexmap, self.units, self.areas, self.countries

    def _init_areas(self):
        from std.util import parse_population
        for area_name, contents in self._areas.items():
            area = Area(self, area_name)
            # set owner
            owner_country_name = self._areas[area_name]['owner']
            country = self.countries[owner_country_name]
            area.owner = country
            area.controller = country

            # set population
            try:
                area.population = parse_population(self._areas[area_name]['population'])
            except KeyError:
                logging.warning(f'No population found for area {area_name}, ignoring...')
            self.areas[area_name] = area

    def _init_countries(self):
        for country_name in self._countries:
            country = Country(self, country_name)
            self.countries[country_name] = country

    def _init_hexmap(self):
        from std.util import parse_coords

        for position_ in self._hexmap:
            try:
                x, y = parse_coords(position_)
            except DataError:
                logging.warning(f'Can not get position {position_}.')
                continue

            position = Position(x, y)

            area_name = self._hexmap[position_]['area']
            self.areas[area_name].add_hex(position)

            # get the hex object at this position
            my_hex = Hex(position)
            my_hex.terrain = self._get_terrain(position_)
            my_hex.controller = self._get_controller_country(position_)
            # put the updated hex object back
            self.hexmap.set_hex(position, my_hex)

    def _init_units(self):
        from std.util import parse_coords
        # import statement required because of 'eval' below

        for position_, unit_ in self._units.items():
            from dn.game.map import unit as unit_module

            try:
                x, y = parse_coords(position_)
            except DataError:
                logging.error(f'Can not get unit position {position_}.')
                continue
            position = Position(x, y)
            country = self.countries[unit_['country']]
            unit_type_ = unit_['type']
            unit_type = eval('unit_module.' + unit_type_)
            if not self.hexmap.is_hex(position):
                raise PositionError(f'no hex  at {position}')
            unit = unit_type(self.world, country, position)
            self.units[position].add(unit)
            logging.info(f'{unit.type} unit for {unit.country} created at {unit.position}')

    def _get_terrain(self, position_: Position) -> TerrainType:
        raw_terrain = self._hexmap[position_]['terrain']
        terrain = TerrainType[raw_terrain]
        return terrain

    def _get_controller_country(self, position: str) -> Country:

        area_name = self._hexmap[position]['area']

        try:
            raw_controller = self._hexmap[position]['controller']
            controller = self.countries[raw_controller]
        except KeyError:  # default controller
            controller = self.areas[area_name].controller
        return controller


class DataError(Exception):
    pass

class DataSyntaxError(DataError):
    pass

class DataValueError(DataError):
    pass
