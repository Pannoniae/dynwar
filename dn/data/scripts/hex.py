""" Convert (slightly) human-readable hex data into program data.

If you see this text, why not try to look around the code? """


# I know, this is a huge mess. Sorry. Intended to be a quick hack, and turned out to be important.

from os.path import sep
from ruamel.yaml import YAML


def lookup_area(area: str):
    try:
        return data['area_mapping'][area]
    except KeyError:
        print('Invalid map %s, please check mapping for definition!' % area)


def lookup_terrain(terrain: str):
    try:
        return data['terrain_mapping'][terrain]
    except KeyError:
        print('Invalid map %s, please check mapping for definition!' % terrain)


def init_hexes():

    global hexes
    hexes = dict()


def convert_areas():
    """ Main body of the function. """
    max_col, max_row = 0, 0
    for index, row in enumerate(data['area']):
        for row_index, hex in enumerate(row):
            hexes['(%s, %s)' % (index, row_index)] = {'area': lookup_area(hex)}
            max_col, max_row = max(index, max_col), max(row_index, max_row)
    print(max_col, max_row)
    global size
    size = max_col + 1, max_row + 1
    yaml.dump({'size_x': size[0]}, output_file)
    yaml.dump({'size_y': size[1]}, output_file)


def convert_terrain():
    for index, row, in enumerate(data['terrain']):
        for row_index, hex in enumerate(row):
            hexes['(%s, %s)' % (index, row_index)]['terrain'] = 'a'
            hexes['(%s, %s)' % (index, row_index)]['terrain'] = lookup_terrain(hex)


if __name__ == '__main__':
    yaml = YAML()
    input_file = open('map' + sep + 'raw_map.yml')
    data = yaml.load(input_file)
    try:
        output_file = open('map.yml', 'r+')
    except FileNotFoundError:
        output_file = open('map.yml', 'w+')
    init_hexes()
    convert_areas()
    convert_terrain()
    yaml.dump(hexes, output_file)
