import unittest

from dn.game.map.hexmap import Hexmap, Direction, Position, TerrainType


class HexmapCase(unittest.TestCase):

    def setUp(self):
        self.hexmap = Hexmap(20, 20)

    def test_correct_number_of_hexes_in_hexmap(self):
        self.assertEqual(len(self.hexmap), 20 * 20)

    def test_hexes_at(self):
        self.assertTrue(self.hexmap.get_hex(Position(0, 1)))  # returns Hex object
        self.assertTrue(self.hexmap.get_hex(Position(100, 50)) is None)  # returns None since there's no hex there

    def test_hex_direction(self):
        self.assertEqual(self.hexmap.hex_direction(Direction.NORTH), Position(0, 1))
        self.assertEqual(self.hexmap.hex_direction(Direction.NORTHEAST), Position(1, 1))
        self.assertEqual(self.hexmap.hex_direction(Direction.SOUTH), Position(0, -1))
        self.assertRaises(IndexError, self.hexmap.hex_direction, 10)

    def test_neighbours(self):
        my_hex = self.hexmap.get_hex(Position(5, 5))
        expected_neighbours = {Position(5, 6), Position(6, 6), Position(6, 5), Position(5, 4), Position(4, 4),
                               Position(4, 5)}
        for hm in self.hexmap.get_all_neighbors(my_hex):
            self.assertTrue(hm.position in expected_neighbours)

    def test_is_adjacent(self):
        my_hex = self.hexmap.get_hex(Position(0, 1))
        adjacent_hex = self.hexmap.get_hex(Position(1, 1))
        self.assertTrue(self.hexmap.is_adjacent(my_hex.position, adjacent_hex.position))
        remote_hex = self.hexmap.get_hex(Position(10, 1))
        self.assertFalse(self.hexmap.is_adjacent(my_hex.position, remote_hex.position))

    def test_get_set_terrain(self):
        my_hex = self.hexmap.get_hex(Position(2, 2))
        self.assertEqual(my_hex.terrain, TerrainType.clear)
        my_hex.terrain = TerrainType.urban
        self.assertEqual(my_hex.terrain, TerrainType.urban)
        with self.assertRaises(ValueError):
            my_hex.terrain = -42  # some nonsense value


    def test_iter(self):
        #  so it doesn't throw an exception
        for hex in self.hexmap:
            pass


if __name__ == '__main__':
    unittest.main()
