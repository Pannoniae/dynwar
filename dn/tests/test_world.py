import unittest

from std.util import parse_population
from dn.game.world import World
from dn.game.map.hexmap import Position
from dn.game.map.terrain import TerrainType, Terrain


class TestWorld(unittest.TestCase):
    def setUp(self):
        self.world = World("test")

    def test_parse(self):
        parsed_population = '306M'

        self.assertEqual(parse_population(parsed_population), 306000000)

    def test_terrain(self):
        self.assertEqual(self.world.hexmap.get_hex(Position(0, 0)).terrain, TerrainType.clear)

    def test_terrain_init(self):
        #  so it doesn't throw an exception
        _ = Terrain()
