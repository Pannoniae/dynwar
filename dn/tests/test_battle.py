""" This is the main test batch, it simulates the world. Actually, it *will* """
from std.util import norandom
from dn.game.map.unit import SupplyState, Infantry
from dn.game.world import World
from dn.game.map.hexmap import Position, Hex
from dn.game.map.terrain import TerrainType

from dn.game.gamestate import GameState

world = None


def setup_module():
    global world
    world = World("test")

def test_battle():


    units = list()
    for pos in range(5):
        unit_pos = Position(6, pos)
        if pos <= 2:
            unit = world.create(world.countries['Northistan'], unit_pos, Infantry)
        else:
            unit = world.create(world.countries['Southistan'], unit_pos, Infantry)
        units.append(unit)

    units[0].supply_state = SupplyState.minor_shortage
    units[1].supply_state = SupplyState.major_shortage

    assert not units[1].can_attack(units[2])  # starving guy cannot attack non-starving guy (also, same nation...)
    assert units[2].can_attack(units[3])  # this is O.K, no one is starving actually

    units[3].health = 2
    units[2].attack(units[3], random_func = norandom)
    assert units[2].health == 8
    assert units[3].health == -2


def test_battle_terrain():
    units = list()

    for pos in range(3):
        unit_pos = Position(6, pos)
        if pos <= 2:
            unit = world.create(world.countries['Northistan'], unit_pos, Infantry)
        else:
            unit = world.create(world.countries['Northistan'], unit_pos, Infantry)
        units.append(unit)

    hex = Hex(world.hexmap, TerrainType.hills)
    world.hexmap.set_hex(Position(6, 0), hex)
    hex = Hex(world.hexmap, TerrainType.urban)
    world.hexmap.set_hex(Position(6, 1), hex)
    hex = Hex(world.hexmap, TerrainType.forest)
    world.hexmap.set_hex(Position(6, 2), hex)

    units[0].attack(units[1], random_func = norandom)
    assert units[1].health == 5.2
    assert units[0].health == 8


def test_mini_battle():
    """ A small battle """
    # TODO
    game = GameState("test")
    #assert len(game.world.units) == 6
    game.run_single_round()
