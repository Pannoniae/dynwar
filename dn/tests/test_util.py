import logging

from pytest import raises

from std import util
from std.util import report
from dn.game.world import World
from dn.io.loader import DataError


def test_parse_coords():
    # OK cases
    assert util.parse_coords("0, 1") == (0, 1)
    assert util.parse_coords("1,1") == (1, 1)

    # Erroneous cases
    with raises(DataError):
        assert util.parse_coords("a ,1") == (1, 1)

    with raises(DataError):
        assert util.parse_coords("1 1") == (1, 1)


def test_parse_population():
    # OK cases
    assert util.parse_population("108") == 108
    assert util.parse_population(" 108") == 108
    assert util.parse_population("108 ") == 108
    assert util.parse_population("108K") == 108000
    assert util.parse_population("108M") == 108000000
    assert util.parse_population("108B") == 108000000000
    assert util.parse_population("108T") == 108000000000000

    # Wrong test cases
    with raises(DataError):
        assert util.parse_population("108X"), 108000

    with raises(DataError):
        assert util.parse_population("-108"), -108
