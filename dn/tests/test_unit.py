import unittest

from dn.game.map.combat import Combat
from dn.game.map.unit import Unit, Infantry
from dn.game.world import World
from std.util import norandom
from dn.game.map.hexmap import Position, Direction


class UnitCase(unittest.TestCase):
    def setUp(self):
        global world
        world = World("scn1")

    def test_can_create_unit(self):
        position = Position(5, 2)
        unit = world.create_unit(world.countries['Northistan'], position)
        unit.health = 5
        self.assertEqual(unit.position, position)
        self.assertEqual(unit.health, 5)

    def test_create_unit_at_empty_hex(self):
        position = Position(3, 3)
        self.assertEqual(len(list(world.get_units_at(position))), 0)
        world.create_unit(world.countries['Northistan'], position)
        self.assertEqual(len(list(world.get_units_at(position))), 1)

    def test_create_another_infantry_at_same_hex(self):
        position = Position(0, 0)
        self.assertEqual(len(list(world.get_units_at(position))), 1)
        world.create_unit(world.countries['Northistan'], position, Infantry)
        self.assertEqual(len(list(world.get_units_at(position))), 2)

    def test_create_baseunit_where_there_is_infantry(self):
        position = Position(0, 0)
        self.assertEqual(len(list(world.get_units_at(position))), 1)
        world.create_unit(world.countries['Northistan'], position)
        self.assertEqual(len(list(world.get_units_at(position))), 2)

    def test_create_baseunit_where_there_is_enemy(self):
        position = Position(5, 5)
        world.create_unit(world.countries['Northistan'], position)
        self.assertEqual(len(list(world.get_units_at(position))), 1)
        world.create(world.countries['Southistan'], position, Unit)
        self.assertEqual(len(list(world.get_units_at(position))), 2)

    def test_can_world_create_unit(self):
        position = Position(5, 5)
        health = 8
        self.assertEqual(len(list(world.get_units_at(position))), 0)
        unit = world.create_unit(world.countries['Northistan'], position)
        unit.health = health
        self.assertEqual(len(list(world.get_units_at(position))), 1)

        self.assertEqual(unit.position, position)
        self.assertEqual(unit.health, health)
        self.assertEqual(list(world.get_units_at(position))[0].position, position)
        self.assertEqual(list(world.get_units_at(position))[0].health, health)
        # direct access still works
        self.assertEqual(world.entities[position].units[0].health, health)
        # can create another unit at the same location
        world.create(world.countries['Southistan'], position, Unit)
        self.assertEqual(len(list(world.get_units_at(position))), 2)


    def test_can_unit_attack_another_country(self):
        unit1 = world.create(world.countries['Northistan'], Position(5, 5), Infantry)
        unit2 = world.create(world.countries['Southistan'], Position(5, 6), Infantry)
        self.assertTrue(unit1.can_attack(unit2))

    def test_can_not_unit_attack_same_country(self):
        unit1 = world.create_unit(world.countries['Northistan'], Position(5, 5), Infantry)
        unit2 = world.create_unit(world.countries['Northistan'], Position(5, 6), Infantry)
        self.assertFalse(unit1.can_attack(unit2))

    def test_cannot_unit_attack(self):
        # non-adjacent hexagons
        unit1 = world.create_unit(world.countries['Northistan'], Position(5, 5), Infantry)
        unit2 = world.create_unit(world.countries['Northistan'], Position(1, 6), Infantry)
        self.assertFalse(unit1.can_attack(unit2))
        # low on health
        world.create(world.countries['Northistan'], Position(1, 5))
        unit1.health = -42
        self.assertFalse(unit1.can_attack(unit2))

    def test_unit_move(self):
        # can move to neighbouring hexmap
        unit = world.create_unit(world.countries['Northistan'], Position(0, 2), Infantry)
        self.assertTrue(unit.can_move(Direction.SOUTH))
        unit.move(Direction.NORTH)
        self.assertEqual(unit.position, Position(0, 3))

        # okay, forget the previous unit, let's create a new one instead

        unit = world.create_unit(world.countries['Northistan'], Position(0, 2), Infantry)
        unit.move(Direction.SOUTH)

    def test_combat_1(self):
        attacker = world.create_unit(world.countries['Northistan'], Position(3, 3), Infantry)
        defender = world.create_unit(world.countries['Northistan'], Position(3, 2), Infantry)
        self.assertEqual(attacker.health, 8)

        c = Combat(attacker, defender, random=norandom)
        c.run()
        # TODO check outcome!
        self.assertEqual(c.defender.health, 4.0)



if __name__ == '__main__':
    unittest.main()
