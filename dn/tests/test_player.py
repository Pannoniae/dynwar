from dn.game.command import MoveUnitCommand
from dn.game.gamestate import GameState
from dn.game.map.hexmap import Direction, Position


def setup():
    global game
    game = GameState('test')


def test_add_player():
    game.add_player('Tom', game.world.countries['Southistan'])
    assert game.players['Tom'].name == 'Tom'


def test_order_of_players():
    game.add_player('Bob', game.world.countries['Southistan'])
    game.add_player('Alice', game.world.countries['Northistan'])
    assert len(game.players) == 2
    assert [player_name for player_name in game.players] == ['Bob', 'Alice']


def test_players_worlds_are_identical():
    game.add_player('Bob', game.world.countries['Northistan'])
    game.add_player('Alice', game.world.countries['Southistan'])
    assert id(game.players['Bob'].world) == id(game.players['Alice'].world)
    assert game.players['Bob'].world == game.players['Alice'].world


def test_player_can_move():
    game.add_player('Bob', game.world.countries['Northistan'])
    player = game.players['Bob']
    unit = list(game.world.get_units_at(Position(0, 0)))[0]
    player.execute(MoveUnitCommand, unit, Direction.NORTH)
    assert unit.position == Position(0, 1)
    ent_cont = game.world.get(Position(0, 1))
    assert ent_cont.units[0].position == Position(0, 1)
    # no unit at old location
    assert game.world.get(Position(0, 0)).units == []


def test_player_can_do_and_undo_move():

    # XXX This is a mess, please clean

    game.add_player('Bob', game.world.countries['Northistan'])
    player = game.players['Bob']
    #print(list(game.world.get_all_units()))
    unit = list(game.world.get_units_at(Position(0, 1)))[0]
    orig_positions = [unit.position for unit in player.get_controlled_units()]
    no_units_before_move = len(orig_positions)
    cmd = player.execute(MoveUnitCommand, unit, Direction.NORTH)
    no_units_after_move = len([unit for unit in player.get_controlled_units()])
    positions_after_move = [unit.position for unit in player.get_controlled_units()]
    assert no_units_before_move == no_units_after_move
    # check the unit is in the right place!
    assert unit.position == Position(0, 1)
    assert positions_after_move != orig_positions
    cmd.undo()
    unit = list(player.get_units_at(Position(0, 0)))[0]
    # the unit is back in the original place
    assert unit.position == Position(0, 0)
    after_undo = len([unit for unit in player.get_controlled_units()])
    assert after_undo == no_units_before_move
    positions_after_undo = [unit.position for unit in player.get_controlled_units()]
    assert positions_after_undo == orig_positions



# TODO
def DISABLED_test_player_complicated_do_turn():
    # specify a bunch of actions and test do_turn
    pass
