import logging
from enum import IntEnum
from queue import PriorityQueue
from typing import Iterator, List, Tuple

from dn.game.map.country import Country
from dn.game.map.terrain import TerrainType, terrain

# possible terrain types

TERRAINS = {i for i in TerrainType}


class PositionError(Exception):
    """ The exception is raised when something is wrong with positions. """
    pass


class Position(object):
    """ A little sidenote. As you could see, Position objects are overused everywhere, even when specifying directions.
        This is because this is basically a Vector class, except that it started as Positions, as I see this name
        perfectly good for most things, only special circumstances don't fit the name. """

    # Borg pattern for memory efficiency:
    __coords__ = {}

    def __new__(cls, x: int, y: int):
        inst = cls.__coords__.get((x, y))
        if inst is None:
            inst = object.__new__(cls)
            inst.x = x
            inst.y = y
            cls.__coords__[(x, y)] = inst
        return inst

    def __init__(self, x: int, y: int) -> None:
        self.x = x
        self.y = y

    def __getitem__(self, item):
        if item == 0:
            return self.x
        elif item == 1:
            return self.y
        else:
            raise PositionError("Positions are 2-dimensional!")

    def __add__(self, other):
        """ position objects can be added as vectors """
        return Position(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        """ position objects can be substracted as vectors """
        return Position(self.x - other.x, self.y - other.y)

    def __neg__(self):
        return Position(-self.x, -self.y)

    def __bool__(self):
        """ allow using Position objects in if conditions """
        return not (self.x == 0 and self.y == 0)

    def __eq__(self, other):
        """ positions are equal if their components are the same """
        return self.x == other.x and self.y == other.y

    def __lt__(self, other):
        """ It doesn't matter, it is just here to statisfy the queue """
        return NotImplemented

    def __hash__(self):
        """row-wise mapping of the two dimensional indices to one dimensional
          needed so that position objects are unique keys in dictionaries
        """
        return self.x * self.y + self.x

    def __repr__(self):
        return f'<position object ({self.x}, {self.y}) at {hex(id(self))}>'

    def __str__(self):
        return f'Position({self.x}, {self.y})'


class Hex(object):
    def __init__(self, position: Position, terrain: TerrainType = TerrainType.clear):
        self._terrain = terrain
        self.position: Position = position

        self._controller = None

    @property
    def terrain(self) -> TerrainType:
        return self._terrain

    @terrain.setter
    def terrain(self, terrain: TerrainType):
        if terrain not in TERRAINS:
            raise ValueError('not a terrain')

        self._terrain = terrain

    @property
    def controller(self) -> 'Country':
        return self._controller

    @controller.setter
    def controller(self, country):
        self._controller = country

    def __str__(self):
        return f'Hex({self.position.x}, {self.position.y})'

    def __repr__(self):
        return f'<hex ({self.position.x}, {self.position.y}) at {hex(id(self))}>'


class Direction(IntEnum):
    """ The enum that holds the direction of hex neighbors.
    Only a helper class, you may use indexes directly as well. """

    # with aliases
    NORTH = N = 0
    NORTHEAST = NE = 1
    SOUTHEAST = SE = 2
    SOUTH = S = 3
    SOUTHWEST = SW = 4
    NORTHWEST = NW = 5


class Hexmap(object):
    def __init__(self, size_x: int, size_y: int):
        self.neighbors = [
            Position(0, +1), Position(+1, +1), Position(+1, 0),
            Position(0, -1), Position(-1, -1), Position(-1, 0)
        ]
        self.hexes = dict()  # Position to Hex
        # fill hexagon grid
        for i in range(0, size_x):
            for j in range(0, size_y):
                position = Position(i, j)
                self.add_hex(position)

    def __len__(self) -> int:
        return len(self.hexes)

    def __iter__(self) -> Iterator[Hex]:
        return iter(self.hexes.values())

    def is_hex(self, position: Position) -> bool:
        assert isinstance(position, Position)
        my_hex = self.hexes[position]
        return my_hex is not None

    def get_hex(self, position: Position) -> Hex:
        assert isinstance(position, Position)
        try:
            return self.hexes[position]
        except KeyError:
            logging.warning(f"Warning: no hex at {position}")

    def add_hex(self, position: Position):
        self.hexes[position] = Hex(position)

    def set_hex(self, position: Position, my_hex: Hex):
        self.hexes[position] = my_hex

    def hex_direction(self, direction: Direction) -> Position:
        """ Returns relative Position. """
        return self.neighbors[direction]

    def get_neighbor(self, hex: Hex, direction: Direction) -> Hex:
        p_direction = self.hex_direction(direction)
        return self.get_hex(hex.position + p_direction)

    def get_all_neighbors(self, hex: Hex) -> Iterator[Hex]:
        for i in Direction:
            yield self.get_neighbor(hex, i)

    def is_adjacent(self, pos: Position, other_pos: Position) -> bool:
        for i in Direction:
            try:
                neighbor_hex = self.get_neighbor(self.get_hex(pos), i)
                if neighbor_hex is None:
                    continue  # we're at the edge
                if neighbor_hex.position == other_pos:
                    return True
            except KeyError:
                # This hex doesn't exist, so ignore it
                pass
        return False

    @staticmethod
    def reverse(direction: Direction) -> Direction:

        # magic numbers are awesome!
        return Direction((direction.value + 3) % 5)

    @staticmethod
    def cost(to_hex: Hex) -> int:
        return terrain.get_movement_cost(to_hex.terrain)

    def find_path(self, start: Position,
                  goal: Position,
                  include_start = True,
                  on_enemy_territory = True) -> Tuple[List[Position], bool]:

        """
        Finds a path between 'start' and 'finish'.
        Returns the path and the success of the operation.
        """
        from std.util import heuristic

        country = self.get_hex(start).controller

        frontier = PriorityQueue()
        frontier.put(start, 0)
        came_from = dict()
        cost_so_far = dict()
        came_from[start] = None
        cost_so_far[start] = 0

        success = False
        while not frontier.empty():
            current = frontier.get()
            current_hex = self.get_hex(current)

            if current == goal:
                success = True
                break
            try:
                for next_hex in self.get_all_neighbors(current_hex):

                    if next_hex.controller != country and not on_enemy_territory:
                        continue

                    next = next_hex.position
                    new_cost = cost_so_far[current] + self.cost(self.get_hex(next))
                    if next not in cost_so_far or new_cost < cost_so_far[next]:
                        cost_so_far[next] = new_cost
                        priority = new_cost + heuristic(goal, next)
                        frontier.put(next, priority)
                        came_from[next] = current
            except AttributeError:
                # so we haven't found a hex here (not in the map)
                pass

        # construct the path from the found came_from dict.

        current = goal
        path = [current]
        while current != start:
            current = came_from[current]
            path.append(current)
        if include_start:
            path.append(start)  # optional
        path.reverse()  # optional

        return path, success
