import logging
from enum import IntEnum
from typing import Union, List, Set, Type

from dn.game.map.country import Country
from dn.game.map.hexmap import Hex, Position, PositionError, Direction
from std.util import normvariate


class EntityContainer(object):

    """ A container which holds several entities. """

    def __init__(self):

        # we can be specific here, as we won't have many slots

        # If you want to add a new special slot, add its typecheck to add() and remove() and ensure that all tests pass.
        self.units: List[Unit] = []
        self._container: Set[Entity] = set()


    def add(self, ent: 'Entity'):
        if isinstance(ent, Unit):
            self.units.append(ent)
        self._container.add(ent)

    def remove(self, ent: 'Entity'):
        if isinstance(ent, Unit):
            self.units.remove(ent)
        self._container.remove(ent)

    def find(self, ent_type: Type['Entity']) -> 'Entity':

        for i in self._container:
            if isinstance(i, ent_type):
                return i

    def find_all(self, ent_type: Type['Entity']) -> List['Entity']:

        ent_list = []
        for i in self._container:
            if isinstance(i, ent_type):
                ent_list.append(i)
        return ent_list

    def __contains__(self, item):
        return item in self._container

    def __repr__(self):
        return f'<EntityContainer object at {hex(id(self))} with {len(self.units)} units>'


class Entity(object):

    """ A thing that lives on the map, like a fortress, city, etc... Basically everything on the map inherits from this. """

    def __init__(self, world, country: Country, position: Position):
        self.world: 'World' = world
        self.hexmap = self.world.hexmap  # the hexagon where this unit lives
        self.position: Position = position  # Position object
        self.country: Country = country  # cuz we are patriots you know

    def __str__(self):
        return f'Entity at {self.position}'

    def __repr__(self):
        return f'<entity object at position {self.position}, at {hex(id(self))}>'

    def copy(self) -> 'Entity':

        target = Entity()
        for attrib in vars(self).items():
            setattr(target, attrib[0], attrib[1])

        return target

    def delete(self):
        self.world.delete(self)


class SupplyState(IntEnum):
    in_supply = 0
    minor_shortage = 1
    moderate_shortage = 2
    major_shortage = 3


class Immovable(Entity):

    """ ABC. An immovable Entity. """


class UnitError(Exception):
    pass

class Unit(Entity):

    def __init__(self, world, country: Country, position: Position):

        super().__init__(world, country, position)

        # Default values, change in subclass ASAP!
        self.health = 10
        self.moves = 5

        self.supply_state: SupplyState = SupplyState.in_supply

        # How much armor this unit has
        self.armor = 0

        # How much dmg this unit can inflict on unarmored targets.
        self.soft_attack = 5

        # How much dmg can this unit deal to armor.
        self.AP = 1
        self.hard_attack = self.AP  # convenience alias

        self.max_health = self.health * 1.0  # we want to copy the value
        self.rem_moves = self.moves

    @property
    def rem_moves(self) -> int:
        return self._remaining_movement

    @rem_moves.setter
    def rem_moves(self, value: int):
        if value < 0:
            raise PositionError('Not enough points for movement!')
        else:
            self._remaining_movement = value

    def can_move(self, arg: Union[Hex, Direction]) -> bool:

        if isinstance(arg, Hex):
            return self.can_move_to(arg)
        elif isinstance(arg, Direction):
            return self.can_move_in(arg)
        else:
            logging.error(f'Invalid argument {arg} passed to can_move!')

    def can_move_in(self, direction: Direction) -> bool:
        target = self.world.hexmap.get_neighbor(self, direction)
        my_hex = self.world.hexmap.get_hex(self.position)
        if target is None or my_hex is None:
            return False
        else:
            return True

    def can_move_to(self, hex: Hex) -> bool:

        """ Slightly more complicated than it seems. Firstly, we have to find a path to that hex,
            nextly we have to check whether we can actually move to there.

            Some awesome stuff, bro! """

        path, success = self.hexmap.find_path(self.position, hex.position)

        if not success:
            return False
        else:
            return True

        # It was simple, ain't it? :)


    def move(self, direction: Direction):
        """ this is an order the player can issue"""

        if self.can_move(direction):
            my_hex = self.world.hexmap.get_hex(self.position)
            to_hex = self.world.hexmap.get_neighbor(my_hex, direction)

            # XXX this was a bug!
            # we just delete the entry at the old position, and set and entry in the new position to this object
            self.world.move(self, to_hex.position, _internal = True)
            self.position = to_hex.position
            cost = self.world.hexmap.cost(to_hex)
            self.rem_moves -= cost
        else:
            logging.info(f"Unit {self} couldn't move towards {direction}")


    def update_moves(self):
        """ Reset remaining movement to the units' maximum movement. """
        self.rem_moves = self.moves

    def can_attack(self, other: 'Unit') -> bool:

        my_hex = self.world.hexmap.get_hex(self.position)
        other_hex = other.world.hexmap.get_hex(other.position)

        if self.country == other.country:  # NKVD bastards
            return False
        if self.health <= 0:
            return False
        if self.supply_state > SupplyState.moderate_shortage:
            return False
        return self.hexmap.is_adjacent(my_hex.position, other_hex.position)

    def attack(self, other_unit, random_func=normvariate):
        """ this is an order the player can issue"""

        from dn.game.map.combat import Combat
        if not self.can_attack(other_unit):
            logging.warning(f"Can't attack position {other_unit.position}!")
        combat = Combat(self, other_unit, random_func)
        combat.run()

    def update_supply(self):

        """ Update supply status. If cut out, worsen supply, else restore supply. """

        # if not encircled: ## TODO
        self.supply_state -= 1
        # else:
        # self.supply_state += 1


    def retreat(self):

        """ TODO complex logic describing where to retreat """

    def __repr__(self):
        klassname = self.__class__.__name__
        return f'<{klassname} object at position {self.position}>'

    @property
    def type(self):
        return self.__class__.__name__


class Infantry(Unit):

    def __init__(self, world, country: Country, position: Position):
        super().__init__(world, country, position)

        self.health = 8
        self.hard_attack = 0.5
        self.soft_attack = 5

class Armor(Unit):

    def __init__(self, world, country: Country, position: Position):
        super().__init__(world, country, position)
        self.health = 15
        self.hard_attack = 8
        self.soft_attack = 10

