from enum import Enum


class TerrainType(Enum):
    """ A terrain. Modifies movement, combat, and several other things. """

    # Main terrains.

    sea = 10
    clear = 20
    mountains = 30
    hills = 40
    forest = 50
    urban = 60
    desert = 70
    jungle = 80

    # Aliases.

    plains = 10

    # Variations.
    urban_sparse = 61
    urban_dense = 62


class Terrain(object):
    """ A collection of functions using TerrainType. """

    def __init__(self):

        self.movement = {
            TerrainType.sea: None,
            TerrainType.clear: 1,
            TerrainType.mountains: 5,
            TerrainType.hills: 2,
            TerrainType.forest: 2,
            TerrainType.urban: 2,
            TerrainType.desert: 1,
            TerrainType.jungle: 4,
            TerrainType.urban_dense: 3
        }

        self.attack = {
            TerrainType.clear: 1.0,
            TerrainType.mountains: 0.5,
            TerrainType.hills: 0.8,
            TerrainType.urban: 0.7,
            TerrainType.jungle: 0.7,
            TerrainType.forest: 0.7
        }

        self.arm_attack = {
            TerrainType.clear: 1.0,
            TerrainType.mountains: 0.5,
            TerrainType.urban: 0.7,
            TerrainType.jungle: 0.6,
            TerrainType.forest: 0.7
        }

        for data in (self.movement, self.attack, self.arm_attack):
            for terrain in TerrainType:
                if terrain not in data:  # so we haven't defined a value
                    if terrain.value % 10 != 0:
                        index = terrain.value - (terrain.value % 10)  # round down to the nearest number ending with 0
                        value = data[TerrainType(index)]
                        data[terrain] = value

    def get_movement_cost(self, terrain_type: TerrainType) -> int:
        return self.movement[terrain_type]

    def get_attack_modifier(self, terrain_type: TerrainType) -> float:
        return self.attack[terrain_type]

    def get_arm_attack_modifier(self, terrain_type: TerrainType) -> float:
        return self.arm_attack[terrain_type]


# a global instance of the class
terrain = Terrain()
