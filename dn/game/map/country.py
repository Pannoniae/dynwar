from typing import Iterator


class Country(object):
    def __init__(self, world, name: str):
        self.name = name
        self.world = world

    def get_all_divisions(self) -> Iterator['Unit']:
        for unit in self.world.units.values():
            if unit.owner == self:
                yield unit

    def get_all_areas(self) -> Iterator['Area']:
        for area in self.world.areas.values():
            if area.owner == self:
                yield area

    def get_all_hexes(self) -> Iterator['Hex']:
        for hex in self.world.hexmap.hexes:
            if hex.controller == self:
                yield hex

    def __eq__(self, other):
        return self.name == other.name and self.world == other.world

    def __str__(self):
        return f'Country {self.name}'

    def __repr__(self):
        return f'<country object named {self.name} at {hex(id(self))}>'
