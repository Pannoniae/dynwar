from typing import Iterator



class Area(object):
    """
    A collection of hexes, usually a part of a country.
    Will be used for scripting and events.
    """
    from dn.game.map.hexmap import Hex, Position

    def __init__(self, world, name: str):
        from dn.game.map.country import Country
        self.name = name

        self.world = world
        self.hexmap = self.world.hexmap
        # It's enough to store the hex positions because the hexagons would take up a lot more memory,
        # and we are premature optimizing
        self.hexes = set()
        # the country this area legally belongs to, aka pre-war situation
        self.owner: Country
        # the country which currently controls this area
        self.controller: Country

        self.population = 0  # the number of idiots here

    def add_hex(self, position: Position):
        # to be called by World which needs to check if position belongs to another country!

        if self.hexmap.is_hex(position):
            self.hexes.add(position)

    def get_all_hexes(self) -> Iterator[Hex]:
        yield from self.hexes

    def __len__(self) -> int:
        # the size of area equals to the number of hexagons
        return len(self.hexes)

    def __str__(self):
        return f'Area, named {self.name}, with {len(self)} hexes'

    def __repr__(self):
        return f'<area {self.name} with len {len(self)} at {hex(id(self))}'
