import logging

from dn.game.map.terrain import terrain
from dn.game.map.unit import Unit, Armor
from std.util import normvariate

class Combat(object):

    def __init__(self, attacker: Unit, defender: Unit, random=normvariate):

        """ A helper class which pins two units against each other, and generates a combat outcome.
        
        It also modifies health, delete units if dead, etc.
        
        """

        self.attacker = attacker
        self.defender = defender

        self.random = random
        self.attack_modifier = None

    def run(self):

        defended_hex = self.defender.hexmap.get_hex(self.defender.position)
        self.attack_modifier = terrain.get_attack_modifier(defended_hex.terrain)
        if isinstance(self.attacker, Armor):
            self.attack_modifier *= terrain.get_arm_attack_modifier(defended_hex.terrain)

        self.attack()
        if self.defender.health <= 0:
            self.defender.delete()
            logging.info(f"{self.defender} died")
        else:
            self.defender.retreat()

    def attack(self):

        """ At first, we multiply the raw attack values by the modifier.
            Then, we will add the random formula in the end.
        """

        attacker_HP_ratio = self.attacker.health / self.attacker.max_health
        if isinstance(self.defender, Armor):
            self.defender.health -= (self.attacker.AP * attacker_HP_ratio * self.attack_modifier) + self.random()
        else:
            self.defender.health -= (self.attacker.soft_attack * attacker_HP_ratio * self.attack_modifier) + self.random()

    def _odds(self):

        """ Getting the hopelessness of the attack.
        
        Usage is that we will adjust damage based on this value, as hopeless attacks into urban won't get successful
        
        UPDATE as of 11/2/2017, this combat model seems enough for now, might revisit later when game is more finished
        
        """
        raise NotImplementedError('Why would you want to use a non-implemented feature?')

