import logging

from dn.game.map.hexmap import Direction
from dn.game.map.unit import Unit
from dn.game.world import World

__all__ = ['Command', 'UnitCommand', 'MoveUnitCommand', 'AttackWithUnitCommand', 'GamePermissionError']


class Command(object):
    """
    Represents a command which can be issued by a Player
    """

    def __init__(self, world: World, player: 'Player'):
        kl = self.__class__.__name__
        self.command_name = kl.lower()  # DO NOT CHANGE MY MODIFICATIONS
        self.world = world
        self.player = player


        # You can get those values if you want to see if user code called super().execute().
        self._exec_called = False
        self._undo_called = False


    def execute(self):
        """ command interface.
        execute command on world on behalf of the player
        """

        # important, as there are things which only base classes handle!
        logging.info('Command executed')
        self._exec_called = True

    def undo(self):
        """ command interface.
        reverse the command
        """
        logging.info('Command undoed')
        self._undo_called = True


class UnitCommand(Command):

    def __init__(self, world: World, player: 'Player', unit: Unit):
        super().__init__(world, player)
        self.unit = unit

    def execute(self):

        super().execute()

        # check if the unit belongs to the player
        if self.unit.country.name != self.player.country.name:
            raise GamePermissionError(
                    f'Player {self.player.name} cannot control unit at {self.unit.position} ' +
                    f"because a {self.unit.country} unit doesn't belong to his country")


class MoveUnitCommand(UnitCommand):
    def __init__(self, world: World, player: 'Player', unit: Unit, direction: Direction):
        super().__init__(world, player, unit)
        self.direction = direction

    def execute(self):

        super().execute()

        if not self.unit.can_move(self.direction):
            logging.error(f'Unit cannot be moved from {self.unit.position} towards {self.direction.name}!')
        else:
            self.unit.move(self.direction)

    def undo(self):

        super().undo()

        reverse_direction = self.world.hexmap.reverse(self.direction)
        self.unit.move(reverse_direction)


class AttackWithUnitCommand(UnitCommand):


    def __init__(self, world: World, player: 'Player', unit: Unit, target_unit: Unit):
        super().__init__(world, player, unit)

        self.target = target_unit

        self._saved_attacker = self.unit.copy()
        self._saved_defender = self.target.copy()

    def execute(self):

        super().execute()

        self.unit.attack(self.target)

    def undo(self):

        super().undo()

        self.world.replace(self.target, self._saved_defender)
        self.world.replace(self.unit, self._saved_attacker)


class GamePermissionError(Exception):
    pass