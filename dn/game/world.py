"""
  The world. What did you expect, sunflowers?
"""
import logging
from collections import defaultdict
from typing import Dict, Iterator

from dn.game.map.unit import Entity, Unit, UnitError, EntityContainer
from dn.io.loader import WorldLoader
from dn.game.map.hexmap import Position, PositionError

class Singleton(type):

    """ Are we crazy? """

    def __call__(cls, *args, **kwargs):
        if not hasattr(cls, '_instance'):
            cls._instance = super().__call__(*args, **kwargs)
            logging.info(f'New instance of {cls.__name__} created.')
        return cls._instance



class World(object, metaclass = Singleton):



    def __init__(self, scenario: str):

        self.scenario = scenario
        # HUAC order. Data should be passed in HUAC order (Hexmap, Units, Areas, Countries) every time. ;)
        self.hexmap = None
        self.entities: Dict[Position, EntityContainer] = defaultdict(EntityContainer)  # positions to entities
        self.areas = dict()  # area name to area
        self.countries = dict()  # country name to country
        self.loader = WorldLoader(self, self.scenario)
        self.players = []
        # _init_ functions
        self.initialize()
        print(list(self.get_all_units()))

    def initialize(self):
        """ initialize our world! """

        self.hexmap, entities, self.areas, self.countries = self.loader.get_values_huac()
        for position in entities:
            # self.entities is a dict of sets, let's change them into entity container
            ent_set = entities[position]
            for i in ent_set:
                self.entities[position].add(i)


    def create(self, country, position: Position, ent_type = Entity) -> Entity:
        if not self.hexmap.is_hex(position):
            raise PositionError('no hex at ', position)
        entity = ent_type(self, country, position)
        self.get(position).add(entity)
        self.update_players('Entity created ', entity_type = ent_type, position = position)
        return entity

    def get(self, position: Position) -> EntityContainer:
        try:
            entities = self.entities[position]
            return entities
        except KeyError:
            logging.warning(f'No EntityContainer at {position}, creating one...')
            self.entities[position] = EntityContainer()
            return self.entities[position]

    def create_unit(self, country, position: Position, ent_type = Unit) -> Unit:
        assert issubclass(ent_type, Unit), 'must be a unit type'
        unit = self.create(country, position, ent_type)
        if not isinstance(unit, Unit):  # okay for subclasses, too
            raise UnitError(f"{unit} couldn't be created")
        self.update_players('Unit created ', unit = unit, position = unit.position)
        return unit

    def add(self, position: Position, ent: Entity):
        self.get(position).add(ent)

    def replace(self, ent: Entity, target: Entity):
        self.delete(ent)
        self.add(ent.position, target)


    def move(self, unit: Unit, target: Position, _internal: bool = False):
        """ Don't use this directly! Use unit.move(), that calls this """
        if not _internal:
            logging.warning('World.move() was used externally, check for errors as it is not safe!')
        self.get(unit.position).remove(unit)
        self.get(target).add(unit)
        self.update_players("Unit moved ", unit = unit, from_position = unit.position, to_position = target)
        logging.info(f'Moved unit {type(unit)} (info {unit}) to {target}')

    def get_all_units(self) -> Iterator[Unit]:
        for pos, ent in self.entities.items():
            for unit in ent.units:
                yield unit

    def get_units_at(self, position: Position) -> Iterator[Unit]:
        # iterator for units at this position
        for unit in self.get_all_units():
            if unit.position == position:
                yield unit

    def delete(self, entity: Entity):
        logging.info(f'Deleting entity at {entity.position}')
        if not self.hexmap.is_hex(entity.position):
            raise PositionError(f'no hex at {entity.position}')
        self.get(entity.position).remove(entity)
        self.update_players("Entity deleted ", entity = entity, position = entity.position)

    def __repr__(self):
        return f'<world at {hex(id(self))}>'

    def __str__(self):
        return f'<world, scenario {self.scenario}>'

    # observer API
    def update_players(self, msg, *args, **kwargs):
        for player in self.players:
            player.update(msg, *args, **kwargs)

    def register_player(self, player):
        if not player in self.players:
            self.players.append(player)
        else:
            logging.warning(f'Player {player} already exists!')

    def unregister_player(self, player):
        if player in self.players:
            self.players.remove(player)
        else:
            logging.warning(f'No player {player} found at World! Ignoring...')

    def unregister_all_players(self):
        if self.players:
            # delete the contents only!
            del self.players[:]
