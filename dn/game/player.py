"""
  Representing the players
"""
import logging
from typing import Optional, Type

from dn.game.command import *
from dn.game.map.country import Country
from dn.game.world import World


# TODO


class Player:
    """ represents a player

    This is an invoker for commands

    It's also an observer for the world!

    """

    command_map: set = {'MoveUnit'}

    def __init__(self, name: str, world: World, country: Country):
        self.name = name
        self.turn = 0
        # runs this country
        self.country = country
        # commands for this particular turn
        self.world = world
        logging.info(f'Player {name} created for {world}')

    def execute(self, command: Type[Command], *args) -> Optional[Command]:  # invoke command
          # is it in the command map?
        if str(command) in self.command_map:

            cmd = command(self.world, self, *args)
            cmd.execute()
            return cmd
        else:
            logging.error(f'An unknown command {command} was issued!')

    def update(self, msg, *args, **kwargs):
        logging.info(f'Player {self.name} was informed: {msg}')
        logging.debug(str(args) + str(kwargs))
        # TODO use this information actually

    def get_controlled_units(self):
        # iterator for units controlled by this player
        for unit in self.world.get_all_units():
            if unit.country == self.country:
                yield unit
