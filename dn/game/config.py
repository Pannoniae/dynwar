from os.path import dirname, join, abspath

BASE_PATH = abspath(dirname(dirname(__file__)))
DATA_PATH = join(BASE_PATH, 'data', 'map')
LOG_PATH = join(BASE_PATH, 'io', 'log')