import calendar
import logging
import os
import time

from collections import OrderedDict

from dn.game.map.country import Country
from std import logging as myLogging

from dn.game.config import LOG_PATH
from dn.game.world import World
from dn.game.player import Player


class InvalidPlayer(Exception):
    pass

class GameState(object):
    """
    The controller class glues the whole piece together by providing a specific instance of the classes.
    """

    def __init__(self, scenario: str):
        self.init_logging()
        self.world = World(scenario)
        self.players = OrderedDict()  # name to player , order DOES matter!!
        self.running = True

    def run(self):
        """ this will be the main game loop """
        while self.running:
            self.run_single_round()
        logging.info("Game stopped!")

    def get_orders_from_player(self, player: Player):
        # TODO
        # input from players to be collected here and added to player.orders
        pass

    def run_single_round(self):

        for player_name, player in self.players.items():
            logging.info(f'Player {player_name} takes his/her turn')
            self.get_orders_from_player(self.players[player_name])

            # TODO
            player.do_turn()

        # clean state for next round
        for ent in self.world.entities.values():
            for unit in ent.units:
                unit.update_supply()
                unit.update_moves()

        logging.info("Round finished")

    def add_player(self, player_name: str, country: Country):
        player = Player(player_name, self.world, country)
        self.players[player_name] = player
        self.world.register_player(player)
        logging.info("Player {} added to {}".format(player_name, self.world))

    def remove_player(self, player_name: str):
        player = self.players[player_name]
        self.world.unregister_player(player)
        logging.info("Player {} removed from {}".format(player_name, self.world))

    @staticmethod
    def init_logging():
        GameState.clear_log_directory()

        logging.basicConfig(
            format = '%(levelname)s: %(filename)s::%(funcName)s(%(lineno)d): %(message)s',
            handlers = [
                myLogging.MyRotatingFileHandler(f"{LOG_PATH}/game.log", maxBytes=2 ** 16),
                logging.StreamHandler(),
                    ],
            level = logging.DEBUG)

    @staticmethod
    def clear_log_directory():
        for file in os.scandir(LOG_PATH):
            if 'log' in file.name:
                try:
                    try:
                        os.remove(file)
                    except FileNotFoundError:
                        pass  # TODO fix it, may not be correct
                except PermissionError:
                    logging.warning(f"Couldn't delete file {file.name}!")
